import copy
def print_m(m):
    print '[',
    for i in xrange(len(m)):
        print '[',
        for j in xrange(len(m[0])):
            if m[i][j] != 0:
                print str(m[i][j]) + "'" + chr(ord('a')+j),
        print ']',
    print ']',
    return

def g_t1(m, k):
    next_m = [[]]*k
    for i in xrange(k):
        next_m[i] = copy.deepcopy(m)
        next_m[i][0][i] +=1
    return False, next_m

def g_t2(m, k):
    next_m = []
    for i in xrange(k):
        if m[0][i] > 0 and (m[2][k] > 0 or m[2][k+1] >0 and (i == 0 or i == k-1)) :
            if m[2][k] > 0: 
                next_m.append(copy.deepcopy(m))
                next_m[-1][0][i] -= 1
                next_m[-1][2][k] -= 1
                next_m[-1][1][k+2] += 1
            if m[2][k+1] > 0 and (i == 0 or i == k - 1):
                next_m.append(copy.deepcopy(m))
                next_m[-1][0][i] -= 1
                next_m[-1][2][k+1] -= 1
                next_m[-1][1][k+3] += 1
            return True, next_m
    return False, next_m

def g_t3(m, k):
    next_m = []
    if reduce( lambda x, y: x + y, m[1] ) > 0:
        for i in xrange(2):
            if m[1][k+2+i] > 0:
                next_m.append(copy.deepcopy(m))
                next_m[-1][1][k+2+i] -= 1
                next_m[-1][2][k+i] += 1
                next_m[-1][3][k+4] += 1
        return True, next_m
    return False, next_m

def g_t4(m, k):
    next_m = []
    if m[3][k+4] > 0:
        next_m.append(copy.deepcopy(m))
        next_m[0][3][k+4] -= 1
        return True, next_m
    return False, next_m

def bilding_of_tree(g, list_of_labels, m, k, n):
    if n == 6:
        return

    for i in xrange(len(g)):
        answer, list_of_m = g[i](m, k)
        if answer:
            for j in xrange(len(list_of_m)):
                    print '\t'*n,'t' + str(i+1) + ':', 
                    print_m(list_of_m[j])
                    if list_of_m[j] in list_of_labels:
                        print 'Iteration M' + str(list_of_labels.index(list_of_m[j]))
                    else:
                        print 'M' + str(len(list_of_labels))
                        list_of_labels.append(list_of_m[j])                 
                        bilding_of_tree(g, list_of_labels, list_of_m[j], k, n+1) 
    return 

if __name__ == "__main__":
    file = open("input_lab3.txt", "r")
    k = int(file.readline())
    m = [[]]*4
    m[0] = map(int, file.readline().split(' '))
    m[1] = [0]*(k+5)
    m[2] = map(int, file.readline().split(' '))
    m[3] = [0]*(k+5)
    g = [g_t1, g_t2, g_t3, g_t4]
    print 'Tree of labels', '\n', 'M0:', 
    print_m(m)
    print 'M0'
    list_of_labels = [m]
    bilding_of_tree(g, list_of_labels, m, k, 1)
    
 


